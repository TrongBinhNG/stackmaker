using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu]
public class Levels : ScriptableObject
{
    public GameObject[] levels;
}
