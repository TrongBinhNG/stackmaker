using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Levels LevelsData;
    private int sceneIndex;
    private void Start()
    {
        Application.targetFrameRate = 60;
        SpawnLevel();
    }
    public  void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public  void NextScene()
    {
        DataManager.Instance.NextLevel();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    void SpawnLevel()
    {
        var curLevel = LevelsData.levels;
        var index = DataManager.Instance.userData.currentLevel;
        if (index > curLevel.Length - 1)
        {
            if (PlayerPrefs.HasKey("levelIndex"))
            {
                index = PlayerPrefs.GetInt("levelIndex");
            }
            else
            {
                index = Random.Range(0, curLevel.Length);
                PlayerPrefs.SetInt("levelIndex", index);
            }
        }
        Instantiate(curLevel[index]);
    }
}