using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/*
 Nguyen Trong Binh
 */

public class DataManager : Singleton<DataManager>
{
    public UserData userData { get; set; }
    public override void OnAwake()
    {
        base.OnAwake();
        LoadUserData();
    }
    public void NextLevel()
    {
        userData.currentLevel += 1;
        SaveUserData();
        PlayerPrefs.DeleteKey("levelIndex");
    }
    public void SaveUserData()
    {
        string json = JsonUtility.ToJson(userData);
        PlayerPrefs.SetString("USERDATA", json);
    }
    private void LoadUserData()
    {
        string json = PlayerPrefs.GetString("USERDATA");
        if (string.IsNullOrEmpty(json))
        {
            userData = new UserData();
        }
        else
        {
            userData = JsonUtility.FromJson<UserData>(json);
        }
    }
    [System.Serializable]
    public class UserData
    {
        public int currentLevel = 0;
    }
}