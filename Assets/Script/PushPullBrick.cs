using UnityEngine;

public class PushPullBrick : MonoBehaviour
{
    [SerializeField] private GameObject pushPullBrick;
    private void OnTriggerEnter(Collider other)
    {
       if(other.tag == "Player" && gameObject.tag == "UnBrick")
        {
            pushPullBrick.SetActive(true);
        }

        if (other.tag == "Player" && gameObject.tag == "Brick")
        {
            pushPullBrick.SetActive(false);
        }
    }


}
